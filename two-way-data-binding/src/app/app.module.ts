import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BananaBoxComponent } from './components/banana-box/banana-box.component';


@NgModule({
  declarations: [
    AppComponent,
    BananaBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    //se añadio el formmodule
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
