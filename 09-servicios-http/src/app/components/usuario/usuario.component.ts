import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  usuario:any;
  usuario2: any;
  usuario3: any;
  femenino: boolean;
  ciudad: boolean;
  usuario4: any;
  usuario5: any;
  constructor(private servicioUsuario: UsuarioService) { 
    this.femenino=false;
    this.ciudad=false;
  }

  ngOnInit(): void {
    this.servicioUsuario.obtenerUsuario().subscribe({
      next: user => {
        // console.log(user);
        this.usuario =user['results'][0]
        // console.log(this.usuario);
      },
      error: error => {
        console.log(error);
      },
      //complete es opcional
      complete: () => {
        console.log('solicitud completa');
      }
    })
 
  }

  showFemale(): void{
    this.servicioUsuario.obtenerUsuarioMasculino().subscribe({
      next: user => {
        // console.log(user);
        this.usuario2 = user['results'][0];
        this.femenino = true;
      },
      error: error => {
        console.log(error);
      }
    });
  }

  //-------------------------------------------
  MostrarElementos(): void {
    this.servicioUsuario.obtenerCantidadElementos().subscribe({
      next: user => {
        // console.log(user);
      },
      error: error => {
        console.log(error);
      }
    });
  }

//Mostramos foto-----------------------------------------
MostrarFoto(): void {
  this.servicioUsuario.obtenerFoto().subscribe({
    next: user => {
      console.log(user);
      this.usuario4 = user[0];
    },
    error: error => {
      console.log(error);
    }
  });
}
//----------------------practica
mostrarDatos(): void {
  this.servicioUsuario.obtenerDatos().subscribe({
    next: user => {
      // console.log(user);
      this.usuario5 = user[0];
    },
    error: error => {
      console.log(error);
    }
  });
}

//-------------------Ciudad !=Canada
  showCountry(): void{
    this.servicioUsuario.obtenerCiudad().subscribe({
      next: user => {
        // console.log(user);
        this.usuario3 = user['results'][0];
        this.ciudad = true;
      },
      error: error => {
        console.log(error);
      }
    });
  }


}
