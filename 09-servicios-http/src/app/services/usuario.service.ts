import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map,take } from "rxjs/operators";



@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  url = 'https://randomuser.me/api/';

  constructor(public http:HttpClient) { }

  obtenerUsuario(): Observable<any>{
    return this.http.get<any>(this.url);
  }

  obtenerUsuarioMasculino():Observable<any>{
    return this.http.get<any>(this.url).pipe(
      filter(usuario =>  usuario['results'][0].gender !='male')
    )
  }
//--------------------------------------------------
  obtenerCantidadElementos(): Observable<any>{
    return this.http.get<any>(this.url).
            pipe(
              take(0)
            );
  }
//-------------map------------------------------------
obtenerFoto(): Observable<any>{
  return this.http.get<any>(this.url).
          pipe(
            map(resp => {
              // console.log(resp);  Este verifica que es la info q nos da
              return resp['results'].map((usuario: any) => {
                console.log(usuario);
                return {
                  name: usuario.name,
                  picture: usuario.picture
                }
              });
            })
          );
}

//---------------------Practica-Datos----------------------
obtenerDatos(): Observable<any>{
  return this.http.get<any>(this.url).
          pipe(
            map(resp => {
              // console.log(resp);  Este verifica que es la info q nos da
              return resp['results'].map((usuario: any) => {
                console.log(usuario);
                return {
                  name: usuario.name,
                  picture: usuario.picture,
                  email: usuario.email,
                  login: usuario.login
                }
              });
            })
          );
}
//-----------Practica ciudad
  obtenerCiudad():Observable<any>{
    return this.http.get<any>(this.url).pipe(
      filter(usuario =>  usuario['results'][0].location['country'] != 'Canada')
    )
  }


}
