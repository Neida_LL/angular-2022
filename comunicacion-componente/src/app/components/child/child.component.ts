import { Component, OnInit ,Input, Output,EventEmitter} from '@angular/core';

// @Input() ChildMesage!:string;

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  
  mensaje='Mensaje del hijo al padre';

  @Input() ChildMessage!:string;
  //Mandamos un mensaje al padre en donde estamos contenidos
  @Output() EventoMensaje = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
    console.log(this.ChildMessage);
    this.EventoMensaje.emit(this.mensaje);
    
  }

}
