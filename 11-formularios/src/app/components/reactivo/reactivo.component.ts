import { Component, OnInit } from '@angular/core';
import { FormArrayName, FormBuilder, FormGroup, Validators ,FormArray, AbstractControlOptions,ValidationErrors} from "@angular/forms";
import { ValidadoresService } from 'src/app/servicios/validadores.service';


@Component({
  selector: 'app-reactivo',
  templateUrl: './reactivo.component.html',
  styleUrls: ['./reactivo.component.css']
})
export class ReactivoComponent implements OnInit {


  forma!: FormGroup;

  constructor( private fb: FormBuilder, private validadores: ValidadoresService) {
    this.crearFormulario();
    this.cargarDataAlFormulario2();
    //this.cargarDataAlFormulario();
   }

  ngOnInit(): void {
  }

  get nombreNoValido(){
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }

  get apellidoNoValido(){
    return (this.forma.get('apellido')?.invalid && !this.forma.get('apellido')?.errors?.['noZaralai']) && this.forma.get('apellido')?.touched;
  }

  get correoNoValido(){
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
  }

  //primera forma de ingresar a los elementos de un grupo:
  get distritoNoValido(){
    return this.forma.get('direccion.distrito')?.invalid && this.forma.get('direccion.distrito')?.touched;
  }

  //otra forma de ingresar a los elementos del grupo:
  get ciudadNoValido(){
    return this.forma.controls['direccion'].get('ciudad')?.invalid && this.forma.controls['direccion'].get('ciudad')?.touched;
  }

  get pasatiempos2(){

    //Convertir = castear;
    return this.forma.get('pasatiempos') as FormArray;
  }


  //------------------------------------------------------
  get apellidoNoZaralai(){
    return this.forma.get('apellido')?.errors?.['noZaralai'];
  }

  get pass1NoValido(){
    return this.forma.get('pass1')?.invalid && this.forma.get('pass1')?.touched;
  }

  get pass2NoValido(){
    return this.forma.get('pass2')?.hasError('noEsigual');
    // const pass1=this.forma.get('pass1')?.value;
    // const pass2=this.forma.get('pass2')?.value;

    // return (pass1==pass2)?false:true;
  }

  get usuarioNoValido(){
   // return this.forma.get('usuario')?.invalid && this.forma.get('usuario')?.touched;
    return (this.forma.get('usuario')?.invalid && !this.forma.get('usuario')?.errors?.['existe']) && this.forma.get('usuario')?.touched;
  }

  get usuarioIgualJerjes(){
    return this.forma.get('usuario')?.errors?.['existe'];
  }

  crearFormulario():void{
    this.forma = this.fb.group({
      //Valores del array:
     //1er valor: El valor por defecto que tendra
     //2do valor: Son los validadores sincronos
     //3er valor: Son los validadores asincronos

     //se puede hacer varios tipos de validacion ,porque no tiene limite:
     nombre:['',[Validators.required,Validators.minLength(4)]],
     apellido:['',[Validators.required, Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/), this.validadores.noZaralai]],
     correo:['',[Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
     //agrupando:
     direccion:this.fb.group({
       distrito:['',Validators.required],
       ciudad:['', Validators.required]
     }),
     pass1:['',Validators.required],
     pass2:['',Validators.required],
     usuario: ['', Validators.required, this.validadores.existeUsuario],
     //arrays => cuando querramos tener formularios dinamicos:
     //generar html dinamico:= a formulario dinamico
     pasatiempos:this.fb.array([[],[],[]]),

    }, {
      validators: this.validadores.passwordsIguales('pass1', 'pass2')
    }  as AbstractControlOptions
);
  }

  cargarDataAlFormulario():void{
  //La estructura debe ser la misma que el json de arriba:
    this.forma.setValue({
      nombre:'Juan',
      apellido:'Perez',
      correo:'juan@gmail.com',
      direccion:{
        distrito:'Central',
        ciudad:'Oruro'
      }
    })
  }

  //Mas usado ,similar al setValue pero es personalizado
  cargarDataAlFormulario2():void{
      this.forma.patchValue({
        apellido:'Perez'
      });
    }

  guardar():void{
    console.log(this.forma.value);
    this.limpiarFormulario();
    
  }

  limpiarFormulario():void{
    //this.forma.reset();
    this.forma.reset({
      nombre:'pedro'
    });

  }

  agregarpasatiempo():void{
    this.pasatiempos2.push(this.fb.control(''));
  }

  borrarPasatiempo(i:number):void{
    this.pasatiempos2.removeAt(i);
  }
}
