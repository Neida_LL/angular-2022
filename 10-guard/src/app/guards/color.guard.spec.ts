import { TestBed } from '@angular/core/testing';

import { ColorGuard } from './color.guard';

describe('ColorGuard', () => {
  let guard: ColorGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ColorGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
