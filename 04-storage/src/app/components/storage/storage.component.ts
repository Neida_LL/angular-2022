import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.css']
})
export class StorageComponent implements OnInit {
 nombre!:string;
 nombre2!:string;
  constructor() { }

  ngOnInit(): void {
    this.nombre='mercedes Sosa';
    this.nombre2 = 'Pedro Meneses';

     //Guardar datos
    //setItem(clave, valor)
    sessionStorage.setItem('nombre', this.nombre);

    //Recuperar datos
    //getItem(clave, valor)
    let resultado = sessionStorage.getItem('nombre');
    console.log(resultado);

    //Remover un item del session storage
    //removeItem(clave)
    //sessionStorage.removeItem('nombre');
    // resultado = sessionStorage.getItem('nombre');
    // console.log(resultado);

    //Limpiar todo el storage
    sessionStorage.clear();
    resultado = sessionStorage.getItem('nombre');
    console.log(resultado);
    
    //Guardar datos
    //setItem(clave, valor)
    localStorage.setItem('nombre2', this.nombre2);

    //Recuperar datos
    //getItem(clave, valor)
    let resultado2 = localStorage.getItem('nombre2');
    console.log(resultado2);


    //Remover un item del session storage
    //removeItem(clave)
    localStorage.removeItem('nombre2');
    resultado = localStorage.getItem('nombre2');
    console.log(resultado);
Luego:
  //Limpiar todo el storage
    localStorage.clear();
    resultado = localStorage.getItem('nombre2');
    console.log(resultado);

  }

}
