import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {

  nombre='Ramiro Santos';
  nombre2= 'MaximiliAno PaRaDes';

  array=[1,2,3,4,5,6,7,8,9,10];
  PI=Math.PI;
  porcentaje=0.234;
  salario=1234.5;
  heroe:any={
    nombre:'Logan',
    clave:'wolverine',
    edad:'500',
    direccion:{
      calle:'Las vertices #478',
      zona:'Los Horizontes'
    },
  }
  //Segunda forma de hacer una promesa_ :
  valorPromesa = new Promise<string>((resolve) => {
    //Espera de 3.5 segundos
    setTimeout(() => {
      resolve('Llego la data');
    }, 3500);
  });

  fecha=new Date();

  constructor() { }

  ngOnInit(): void {
  }

}
