import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizado'
})
export class CapitalizadoPipe implements PipeTransform {

  transform(value: string,todas:boolean=true): string {
    console.log(value);

    value= value.toLowerCase();
    let nombres = value.split(' ');
    
    if(todas){
      nombres = nombres.map(nombre => {
        return nombre[0].toUpperCase() + nombre.substring(1);
        //cambios y sobreescribe el map para arrays
      });
    } else {
      console.log(nombres);
      console.log(nombres[0][0].toUpperCase());
      // nombres[0][0].toUpperCase() el primer [0] obtiene el elemento de la posicion 0, el segundo [0]
      // obtiene el caracter de la posicion 0
      nombres[0] = nombres[0][0].toUpperCase() + nombres[0].substring(6);
    }

    return nombres.join(' ');

  }

}
