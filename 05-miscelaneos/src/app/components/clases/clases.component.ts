import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clases',
  templateUrl: './clases.component.html',
  styleUrls: ['./clases.component.css']
})
export class ClasesComponent implements OnInit {

  alerta = 'alerta';
  propiedades = {
    error: false
  }
  loading = false;
  ejecutar(): void{
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
    }, 3000);
  }


  constructor() { }

  ngOnInit(): void {
  }

}
