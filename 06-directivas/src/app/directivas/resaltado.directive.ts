import { Directive ,ElementRef, HostListener,Input} from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {
  @Input("appResaltado")nuevoColor!:string;


  //inyeccion de dependencias
  constructor(private el:ElementRef) { 
    console.log('Llamado de directiva');
  //  el.nativeElement.style.backgroundColor='yellow';
  }


  //Quiero escuchar en este caso el evento mouse enter
  @HostListener('mouseenter')mouseEntro(){
    // console.log(this.nuevoColor);
    
    // this.el.nativeElement.style.backgroundColor='yellow';
    this.resaltar(this.nuevoColor || 'yellow');

  }

  //Quiero escuchar en este caso el evento mouse leave
  @HostListener('mouseleave')mouseSalio(){
    // this.el.nativeElement.style.backgroundColor=null;
    this.resaltar(null!);

  }

  private resaltar(color: string): void{
    this.el.nativeElement.style.backgroundColor = color;
  }


}
