import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from "@angular/router";

import { Heroe, HeroesService } from "../../servicios/heroes.service";@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

    heroes:any[]=[];
    //pide que inicialice, pero le ponemos negacion y funciona de maravilla
    termino!:string;


  constructor(private activatedRoute: ActivatedRoute,
 	private router: Router, private heroesService: HeroesService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      console.log(params['termino']);

      this.termino= params['termino'];
      this.heroes = this.heroesService.buscarHeroes(this.termino);

      console.log(this.heroes);

    });

  }

  verHeroe(idx: number){
    console.log(idx);
    this.router.navigate(['/heroe', idx]);
  }



}
