import { Component, OnInit } from '@angular/core';
import { HeroesService ,Heroe} from "../../servicios/heroes.service";
import { Router } from "@angular/router";
@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes:Heroe[] =[];

  constructor(private _heroes: HeroesService, private router:Router) {     
    console.log('constructor');
  }

  ngOnInit(): void {
    console.log('ngOnInit');
    this.heroes =this._heroes.getHeroes();
    console.log(this.heroes);
    
  }

    verHeroe(idx:number):void {
      console.log(idx);
      this.router.navigate(['/heroe',idx]);
    }
}

